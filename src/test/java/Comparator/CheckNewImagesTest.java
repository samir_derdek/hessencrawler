package Comparator;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import javax.swing.text.ChangedCharSetException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Test;
import org.xml.sax.SAXException;

public class CheckNewImagesTest {

	@Test
	public void test() throws ParserConfigurationException, SAXException, XPathExpressionException, SQLException, IOException {
		CheckNewImages.check("target/HessenBooks.txt");
	}

}
