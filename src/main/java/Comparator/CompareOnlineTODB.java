package Comparator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

public class CompareOnlineTODB {

	private static HashMap<String,ArrayList<String>> loadFile(String path) throws NumberFormatException, IOException{
		String line="";
		BufferedReader reader = Files.newBufferedReader(Paths.get(path));
		HashMap<String,ArrayList<String>> list = new HashMap<String,ArrayList<String>>();
//		reader.readLine();
		while((line = reader.readLine())!=null){
			try{
			String id2=line.split(";")[0];
			String externId=line.split(";")[1];
			if(list.containsKey(id2)){
				list.get(id2).add(externId);
			}else{
				ArrayList<String> externList = new ArrayList<String>();
				externList.add(externId);
				list.put(id2, externList);
			}
			}
			catch(IndexOutOfBoundsException e){
				System.out.println("Exception on : "+line);
			}
			
		}
		return list;
	}
	
	
	public static void main(String[] args) throws NumberFormatException, IOException{
		
		
		HashMap<String,ArrayList<String>> WEB=loadFile("C:/Users/samir/Documents/COLLECTIONS/HESSEN/february/diffMars.txt");
		System.out.println("DB charged");
		HashMap<String,ArrayList<String>> DB=loadFile("C:/Users/samir/Documents/COLLECTIONS/HESSEN/february/diffFebruary.txt");
		System.out.println("Online charged");
		HashMap<String,ArrayList<String>> DIFF=new HashMap<String, ArrayList<String>>();
		System.out.println(WEB.keySet().size());
		System.out.println(DB.keySet().size());
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("C:/Users/samir/Documents/COLLECTIONS/HESSEN/february/rest.txt"))) {
			writer.write("id2,extern_id\n");
			for(String id2 : DB.keySet()){
			if(WEB.containsKey(id2)){
				ArrayList<String> diff = new ArrayList<String>(WEB.get(id2));
				diff.removeAll(DB.get(id2));
				DIFF.put(id2, diff);
				for(String externId : diff)
					writer.write(id2+";"+externId+"\n");
			}
		}}
		System.out.println(DIFF);
	}
}
