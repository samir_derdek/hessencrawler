package Comparator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import utils.CrawlerDocument;
import utils.CrawlerService;

public class CheckNewImages {
	
	private final static String IURL="http://digitalisate.hadis.hessen.de/hstamr/";
	static BufferedReader br = null;
	static CrawlerService crawlerService = new CrawlerService();
	
	public static void check(String path) throws ParserConfigurationException, SAXException, XPathExpressionException, SQLException, IOException{
		
		//Write ou results (books where number of images is not matching !)
		FileWriter fw = new FileWriter(new File("target/comparisonFebruar.csv"));
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("id2;externId;Database;HessenWebsite;Status\n");
		
		
		//Open and prepare database
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager
					.getConnection("jdbc:postgresql://db01.qidenus.com:5432/books",
							"samir", "quidenirsam");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully");
		Statement stmt = c.createStatement();
		
		try {

			String currentLine;
			br = new BufferedReader(new FileReader(path));

			while ((currentLine = br.readLine()) != null) {
				String id2=currentLine.split(";")[0];
				String externId=currentLine.split(";")[1];
				int numPages=0;
//				try{
//					numPages=Integer.parseInt(currentLine.split(";")[37]);
//				}catch(NumberFormatException e){
//				}
				String currentURL=IURL+id2+"/"+externId+".xml";
				CrawlerDocument doc=null;
				
				String status;
				try{
					doc = new CrawlerDocument(currentURL);
				}
				catch (org.jsoup.HttpStatusException e){
					System.err.println(currentURL+" doesn't exist !");
					status="book doesn't exist online";
				}
				if(doc!=null){
					String query = "SELECT (SELECT COUNT(*) FROM pages WHERE pages.book_id = books.id) AS NumPagesCountedInPagesTable FROM books WHERE id2="+"\'"+id2+"\' AND extern_id="+"\'"+externId+"\'";
					ResultSet rs = stmt.executeQuery(query);
					rs.next();
					numPages=rs.getInt(1);
					//Get number of pages on the server
					System.out.println(crawlerService.selectByTag(doc, "mods:title").text());
					int numberOfPagesOnline=0;
					try{
					numberOfPagesOnline = Integer.parseInt(crawlerService.selectByTag(doc, "mets:FLocat").last().toString()
							.split("<mets:flocat loctype=\"URL\" xlink:href=\"")[1].split("/")[7]
									.replaceAll(".jpg", "").split("\"")[0].replaceFirst("^0+(?!$)", ""));
					}
					catch(NumberFormatException e){
						
					}
					
					System.out.println("Number of pages : "+numberOfPagesOnline);
					//Get number of pages into the database
					
					//System.out.println("Into the database are "+numberOfPagesInDatabase+" images.");
					
					if(numPages==numberOfPagesOnline){
						status= "match";
					}else{
						if(numPages==(2*numberOfPagesOnline)){
							status="duplicated";
						}
						else{
							status="no match";
						}
					}
					bw.write(id2+";"+externId+";"+numPages+";"+numberOfPagesOnline+";"+status+"\n");
					bw.flush();
				}
			}
			bw.close();
			c.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
