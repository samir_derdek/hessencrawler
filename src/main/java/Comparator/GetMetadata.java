package Comparator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.SAXException;
import utils.CrawlerDocument;
import utils.CrawlerService;
/**
 * Crawl metadata from hessen archive website
 * @author samir
 *
 */
public class GetMetadata {

	private final static String IURL="http://digitalisate.hadis.hessen.de/hstamr/";

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		CrawlerService crawlerService= new CrawlerService();

		int countTotal=1,countTFB=1,countSTB=1,countTRB=1;
		FileOutputStream fos = new FileOutputStream(new File("target/Hessen_06_03_2017.xlsx"));
		XSSFWorkbook myWorkBook = utils.Utils.createMetadataFile(fos);
		XSSFRow currentRow = null,currentRowGesamt=null;

//		try (InputStreamReader isr = new InputStreamReader(new FileInputStream("src/main/resources/Diff_numberMars.csv"), Charset.forName("UTF-8"));
				try (InputStreamReader isr = new InputStreamReader(new FileInputStream("src/main/resources/Diff_numberMars_reduced.csv"), Charset.forName("UTF-8"));

				BufferedReader br = new BufferedReader(isr);
				){
			br.readLine();
			String line;
			while ((line = br.readLine()) != null){
				String id2 = line.split(",")[0];
				String externId = line.split(",")[1];
				String currentURL=IURL+id2+"/"+externId+".xml";
				int numPages = 0;
				CrawlerDocument doc=null;
				String status;
				try{
					doc = new CrawlerDocument(currentURL);
					String bookName = crawlerService.selectByTag(doc, "mods:title").text();
					String place = crawlerService.selectByTag(doc, "mods:placeTerm").text();
					String date = crawlerService.selectByTag(doc, "mods:dateIssued").text();
					numPages = Integer.parseInt(crawlerService.selectByTag(doc, "mets:FLocat").last().toString()
							.split("<mets:flocat loctype=\"URL\" xlink:href=\"")[1].split("/")[7]
									.replaceAll(".jpg", "").split("\"")[0].replaceFirst("^0+(?!$)", ""));
					String dateFrom=date;
					String dateTo=date;
					if(date.contains("-")){
						dateFrom=date.split("-")[0];
						dateTo=date.split("-")[0];
					}
					
					System.out.println("Book name : "+bookName);
					String docType="?";
					if(bookName.contains("Sterbe")){
						docType="Death";
						utils.Utils.createCell(myWorkBook, numPages, id2, externId, dateFrom,dateTo, bookName, place, "Death",docType, countSTB);
						countSTB++;
					}else{
						if(bookName.contains("Heirat")){
							docType="Marriage";
							utils.Utils.createCell(myWorkBook, numPages, id2, externId,dateFrom,dateTo, bookName, place, "Marriage",docType, countTRB);
							countTRB++;
						}else{
							if(bookName.contains("Geburt")){
								docType="Birth";
								utils.Utils.createCell(myWorkBook, numPages, id2, externId, dateFrom,dateTo, bookName, place, "Birth",docType, countTFB);
								countTFB++;
							}
						}
					}
					utils.Utils.createCell(myWorkBook, numPages, id2, externId, dateFrom,dateTo, bookName, place, "All",docType, countTotal);
					countTotal++;
				}
				catch (java.net.ConnectException e){
					System.err.println("Time out for : "+ currentURL);
				}
			}	
		}
		myWorkBook.write(fos);
		fos.flush();
		fos.close();
	}
}
