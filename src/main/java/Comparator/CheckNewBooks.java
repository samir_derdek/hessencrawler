package Comparator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Authenticator;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import utils.ProxyAuthenticator;

import utils.Book;
import utils.CrawlerDocument;

public class CheckNewBooks {

	private final static String IURL="http://digitalisate.hadis.hessen.de/hstamr/";
	ArrayList<Book> missingBooks;
	private final static Path path = Paths.get("target/OnlineBooks_test.txt");
	
	private static ArrayList<String> loadProxies(String path) throws NumberFormatException, IOException{
		String line="";
		BufferedReader reader = Files.newBufferedReader(Paths.get(path));
		ArrayList<String> proxies = new ArrayList<String>();
		reader.readLine();
		while((line = reader.readLine())!=null){
			proxies.add(line.split(":")[0]);
		}
		return proxies;
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		Authenticator.setDefault(new utils.ProxyAuthenticator("sdorfm", "cXzWCnym"));
		ArrayList<String> proxies = loadProxies("src/main/resources/proxyList.txt");
		int i=0,proxyPort=29842;
		
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {


			for(int id2=926;id2<928;id2++){
				for(int externId=0;externId<2000;externId++){
					int numPages=0;
					String currentURL=IURL+id2+"/"+externId+".xml";
					CrawlerDocument doc=null;

					String status;
					try{
						doc = new CrawlerDocument(currentURL/*,proxies.get(i),proxyPort*/);
						writer.write(id2+";"+externId+"\n");
						System.out.println(id2+";"+externId);
					}
					catch (org.jsoup.HttpStatusException e){
//						System.err.println(currentURL+" doesn't exist !");
//						status="book doesn't exist online";
					}
					if(i<49){
						i++;
					}
					else{
						i=0;
						System.out.println(id2+"/"+externId);
					}
				}
			}	
		}			
	}
}
