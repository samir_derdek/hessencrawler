package Comparator;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.stream.Stream;

import utils.Book;

/**
 * Compare online books and booksDB ones
 * @author samir
 *
 */
public class CompareFiles {

		public static void main(String[] args) throws FileNotFoundException, IOException{

			HashSet<Book> booksDB = new HashSet<Book>();
			HashSet<Book> booksDIR = new HashSet<Book>();
			String line;
			try (InputStreamReader isr = new InputStreamReader(new FileInputStream("src/main/resources/BooksHessenDB.csv"), Charset.forName("UTF-8"));
					BufferedReader br = new BufferedReader(isr);
					){
				while ((line = br.readLine()) != null) {
					booksDB.add(new Book(line.split(";")[0],line.split(";")[1],Integer.parseInt(line.split(";")[2])));
				}
			}
			System.out.println("Books DB loaded ....");
			
			try (InputStreamReader isr = new InputStreamReader(new FileInputStream("target/onlineBooks.txt"), Charset.forName("UTF-8"));
					BufferedReader br = new BufferedReader(isr);
					){
				while ((line = br.readLine()) != null) {
					booksDIR.add(new Book(line.split(";")[0],line.split(";")[1],Integer.parseInt(line.split(";")[2])));
				}
			}
			
			System.out.println("Online DB loaded ....");
			System.out.println("Comparing both ...");
			
			try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("target/missingIntoDB.txt"))) {
				writer.write("id2,extern_id,num_pagesDIR,num_pagesDB\n");
			for(Book bookDIR : booksDIR){
				for(Book bookDB : booksDB){
					if((bookDB.getId2().equals(bookDIR.getId2())) && ((bookDB.getExternId().equals(bookDIR.getExternId())))){
						if(bookDB.getNumPages()!=bookDIR.getNumPages())
							writer.write(bookDB.getId2()+","+bookDB.getExternId()+","+bookDIR.getNumPages()+","+bookDB.getNumPages()+"\n");
					}
					}
				}
			}
			System.out.println("Results write out into : /target/missingIntoDB.txt");
			
		}
}
