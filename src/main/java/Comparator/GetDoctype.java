package Comparator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.google.common.collect.ArrayListMultimap;

import utils.Book;
import utils.CrawlerDocument;
import utils.CrawlerService;

/**
 * Crawl images from hessen archive website
 * @author samir
 *
 */

public class GetDoctype {

	private final static String IURL="http://digitalisate.hadis.hessen.de/hstamr/";
	ArrayList<Book> missingBooks;
//	private final static Path path = Paths.get("target/Diff_numberMars.csv");


	private static ArrayList<String> loadProxies(String path) throws NumberFormatException, IOException{
		String line="";
		BufferedReader reader = Files.newBufferedReader(Paths.get(path));
		ArrayList<String> proxies = new ArrayList<String>();
		reader.readLine();
		while((line = reader.readLine())!=null){
			proxies.add(line.split(":")[0]);
		}
		return proxies;
	}

	private static ArrayListMultimap<String, String> loadBooks(String path) throws IOException{
		ArrayListMultimap<String, String> books = ArrayListMultimap.create();
		String line=null;
		try (InputStreamReader isr = new InputStreamReader(new FileInputStream(path), Charset.forName("UTF-8"));
				BufferedReader br = new BufferedReader(isr);
				){
			br.readLine();
			while ((line = br.readLine()) != null){
				books.put(line.split(",")[0],line.split(",")[1]);	
			}
		}
		return books;
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		Authenticator.setDefault(new utils.ProxyAuthenticator("sdorfm", "cXzWCnym"));

		CrawlerService crawlerService= new CrawlerService();
		boolean ok=false;
		//		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
		//			writer.write("id2,externId,docType,numPages\n");

		try (InputStreamReader isr = new InputStreamReader(new FileInputStream("src/main/resources/Diff_numberMars.csv"), Charset.forName("UTF-8"));
				BufferedReader br = new BufferedReader(isr);
				){
			br.readLine();
			String line;
			while ((line = br.readLine()) != null){
				String id2 = line.split(",")[0];
				String externId = line.split(",")[1];	
//				String id2 = "900";
//				String externId="7367";
				String currentURL=IURL+id2+"/"+externId+".xml";
				CrawlerDocument doc=null;
//				if(id2.equals("900")&&externId.equals("5577")){
//					ok=true;
//				}
				if(true){
					String status;
					try{
						doc = new CrawlerDocument(currentURL);
						String bookName = crawlerService.selectByTag(doc, "mods:title").text();
						//						System.out.println("Book name : "+bookName);
						String docType="dontKnow";
						if(bookName.toLowerCase().contains("sterbe")){
							docType="DEATH";
						}else{
							if(bookName.toLowerCase().contains("heirat")){
								docType="MARRIAGE";
							}else{
								if(bookName.toLowerCase().contains("geburt")){
									docType="BIRTH";
								}
							}
						}
						//writer.write(id2+","+externId+","+docType+","+line.split(",")[5]+"\n");
						int numPages = Integer.parseInt(crawlerService.selectByTag(doc, "mets:FLocat").last().toString()
								.split("<mets:flocat loctype=\"URL\" xlink:href=\"")[1].split("/")[7]
										.replaceAll(".jpg", "").split("\"")[0].replaceFirst("^0+(?!$)", ""));
						for(int i=1;i<=numPages;i++){
							String imageName=String.format("%05d", i)+".jpg";
							String imageUrl = IURL+id2+"/"+externId+"/max/"+imageName;
							crawlerService.savePic(imageUrl, imageName, "target/crawled_images/"+docType+"/"+id2+"/"+externId);
						}
						//						System.out.println(id2+";"+externId);
					}
					catch (java.net.ConnectException e){
						System.err.println("Time out for : "+ currentURL);
					}
				}
			}	
		}		
		//		}
	}
}
