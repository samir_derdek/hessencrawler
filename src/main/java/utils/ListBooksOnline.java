package utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 * List all available books online
 * @author samir
 *
 */
public class ListBooksOnline {

	private final static String IURL="http://digitalisate.hadis.hessen.de/hstamr/";
	ArrayList<Book> missingBooks;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		ArrayList<String> id2List = new ArrayList<String>(Arrays.asList("900","903","904","905","908","909","911","912","913","916","918","921","922","923","924","925","926")); 
		Path path = Paths.get("target/onlineBooks.txt");
		CrawlerService crawlerService = new CrawlerService();
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
		// for(int id2=900;id2<930;id2++){
			for(int id2=900;id2<901;id2++){
			if(id2List.contains(""+id2)){
			// for(int externId=1;externId<15000;externId++){
			for(int externId=1;externId<100;externId++){
				int numPages=0;
				String currentURL=IURL+id2+"/"+externId+".xml";
				CrawlerDocument doc=null;
				
				String status;
				try{
					doc = new CrawlerDocument(currentURL);
					int numberOfPagesOnline=0;
						try{
							numberOfPagesOnline = Integer.parseInt(crawlerService.selectByTag(doc, "mets:FLocat").last().toString()
								.split("<mets:flocat loctype=\"URL\" xlink:href=\"")[1].split("/")[7]
										.replaceAll(".jpg", "").split("\"")[0].replaceFirst("^0+(?!$)", ""));
						}
						catch(NumberFormatException e){
							
						}
						writer.write(id2+";"+externId+";"+numberOfPagesOnline+"\n");
				}
				catch (org.jsoup.HttpStatusException e){
					System.err.println(currentURL+" doesn't exist !");
					status="book doesn't exist online";
				}
			}}
		}
		}
		System.out.println("List write out into : /target/onlineBooks.txt");
	}
}
