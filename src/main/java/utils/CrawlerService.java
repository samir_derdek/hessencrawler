package utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Allow to query a CrawlerDocument
 * @author samir
 *
 */
public class CrawlerService {

	Log logger = LogFactory.getLog(getClass().getName());

	public Elements select(CrawlerDocument doc, String cssQuery){
		return doc.getDoc().select(cssQuery);
	}
	
	public Elements selectByTag(CrawlerDocument doc, String tagName){
		return doc.getDoc().getElementsByTag(tagName);
	}

	public void savePic(String imageUrl,String imageName, String destinationPath) throws IOException{
		URL url=null;
		File fTest = new File(destinationPath);
		if(!(fTest.exists())){
			fTest.mkdirs();
		}
		try {
			url = new URL(imageUrl);
			ImageIO.write(ImageIO.read(url), "jpg", new File(destinationPath+"/"+imageName));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error("IllegalArgumentException fail on : "+url);
		}
		catch(IOException ie){
			logger.error("IOException fail on : "+url);
			url = new URL(imageUrl);
//			try{ImageIO.write(ImageIO.read(url), "jpg", new File(destinationPath+"/"+imageName));
		}
		System.out.println("File saved : "+ destinationPath+"/"+imageName);
	}

	public String parse(CrawlerDocument doc, String attributeName) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException{
		DocumentBuilderFactory factory =
		DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		StringBuilder xmlStringBuilder = new StringBuilder();
		xmlStringBuilder.append(doc.getDoc().text());
		ByteArrayInputStream input =  new ByteArrayInputStream(
		   xmlStringBuilder.toString().getBytes("UTF-8"));
		Document parsedDoc = builder.parse(input);
		Element root = parsedDoc.getDocumentElement();
		return root.getAttribute(attributeName);
	}

}