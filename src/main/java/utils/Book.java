package utils;

public class Book {

	private String id2;
	private String externId;
	private int numPages;
	
	public Book(String id2,String externId,int numPages){
		this.externId=externId;
		this.id2=id2;
		this.numPages=numPages;
	}

	public String getId2() {
		return id2;
	}

	public void setId2(String id2) {
		this.id2 = id2;
	}

	public String getExternId() {
		return externId;
	}

	public void setExternId(String externId) {
		this.externId = externId;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	@Override
	public String toString() {
		return "Book [id2=" + id2 + ", externId=" + externId + ", numPages=" + numPages + "]";
	}
	
}
