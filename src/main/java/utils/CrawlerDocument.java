package utils;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xml.sax.SAXException;

/**
 * Contains all needed infos for processing a web page
 * @author samir
 *
 */
public class CrawlerDocument {

	private String url;
	private Document doc;

	public CrawlerDocument(String url) throws ParserConfigurationException, SAXException, IOException{
		doc = Jsoup.parse(new URL(url), 100000000);
		
		this.url=url;
	}
public CrawlerDocument(String url, String host, int port) throws ParserConfigurationException, SAXException, IOException{
		
//		// if you use https, set it here too
//		System.setPropertyhost, "<proxyip>"); // set proxy server
//		System.setProperty("http.proxyPort", "<proxyport>"); // set proxy port
//
//		doc = Jsoup.connect("http://your.url.here").get(); // Jsoup now connects via proxy
	try{
		Jsoup.connect(url).timeout(1000000).proxy(host, port).get();
	}catch(ConnectException e){
		
	}
//		logger.info(url);
		doc = Jsoup.parse(new URL(url), 1000000);
		this.url=url;
	}


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

}
