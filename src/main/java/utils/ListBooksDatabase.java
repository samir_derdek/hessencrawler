package utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ListBooksDatabase {

	private final static int COLLECTION_ID=1;

	public static void main(String[] args)throws SQLException, IOException{
		ArrayList<Book> listBooks = new ArrayList<Book>();

		Path path = Paths.get("target/hessenBooks.txt");
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager
					.getConnection("jdbc:postgresql://db01.qidenus.com:5432/books",
							"samir", "quidenirsam");
			c.setAutoCommit(false);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully");
		String bookQuery="SELECT books.id2, books.extern_id, (SELECT COUNT(*) FROM pages WHERE pages.book_id = books.id) AS NumPages FROM books WHERE collection_id="+COLLECTION_ID;
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(bookQuery);
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
				writer.write("id2;extern_id;num_pages\n");
			while(rs.next()){
				writer.write(rs.getString(1)+";"+rs.getString(2)+";"+rs.getInt(3)+"\n");
			}
		}
	}
}
