package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utils {

	public static XSSFWorkbook createMetadataFile(FileOutputStream fos){
		XSSFWorkbook myWorkBook = new XSSFWorkbook ();

		//Create sheets
		XSSFSheet sheetGesamt = myWorkBook.createSheet("All");
		XSSFSheet sheetTFB = myWorkBook.createSheet("Birth");
		XSSFSheet sheetTRB = myWorkBook.createSheet("Marriage");
		XSSFSheet sheetSTB = myWorkBook.createSheet("Death");

		//Create first rows
		XSSFRow firstRowGesamt = sheetGesamt.createRow(0);
		XSSFRow firstRowTFB = sheetTFB.createRow(0);
		XSSFRow firstRowTRB = sheetTRB.createRow(0);
		XSSFRow firstRowSTB = sheetSTB.createRow(0);


		firstRowGesamt.createCell(0).setCellValue("HSTAM Number");
		firstRowGesamt.createCell(1).setCellValue("Pfarre Name");
		firstRowGesamt.createCell(2).setCellValue("Signatur");
		firstRowGesamt.createCell(3).setCellValue("Book Name");
		firstRowGesamt.createCell(4).setCellValue("Date from");
		firstRowGesamt.createCell(5).setCellValue("Date to");
		firstRowGesamt.createCell(6).setCellValue("Period");
		firstRowGesamt.createCell(7).setCellValue("DocType");
		firstRowGesamt.createCell(8).setCellValue("Num images");
		firstRowGesamt.createCell(9).setCellValue("Restricted");
		firstRowGesamt.createCell(10).setCellValue("Path");

		firstRowTFB.createCell(0).setCellValue("HSTAM Number");
		firstRowTFB.createCell(1).setCellValue("Pfarre Name");
		firstRowTFB.createCell(2).setCellValue("Signatur");
		firstRowTFB.createCell(3).setCellValue("Book Name");
		firstRowTFB.createCell(4).setCellValue("Date from");
		firstRowTFB.createCell(5).setCellValue("Date to");
		firstRowTFB.createCell(6).setCellValue("Period");
		firstRowTFB.createCell(7).setCellValue("DocType");
		firstRowTFB.createCell(8).setCellValue("Num images");		
		firstRowTFB.createCell(9).setCellValue("Restricted");
		firstRowTFB.createCell(10).setCellValue("Path");

		firstRowTRB.createCell(0).setCellValue("HSTAM Number");
		firstRowTRB.createCell(1).setCellValue("Pfarre Name");
		firstRowTRB.createCell(2).setCellValue("Signatur");
		firstRowTRB.createCell(3).setCellValue("Book Name");
		firstRowTRB.createCell(4).setCellValue("Date from");
		firstRowTRB.createCell(5).setCellValue("Date to");
		firstRowTRB.createCell(6).setCellValue("Period");
		firstRowTRB.createCell(7).setCellValue("DocType");
		firstRowTRB.createCell(8).setCellValue("Num images");
		firstRowTRB.createCell(9).setCellValue("Restricted");
		firstRowTRB.createCell(10).setCellValue("Path");

		firstRowSTB.createCell(0).setCellValue("HSTAM Number");
		firstRowSTB.createCell(1).setCellValue("Pfarre Name");
		firstRowSTB.createCell(2).setCellValue("Signatur");
		firstRowSTB.createCell(3).setCellValue("Book Name");
		firstRowSTB.createCell(4).setCellValue("Date from");
		firstRowSTB.createCell(5).setCellValue("Date to");
		firstRowSTB.createCell(6).setCellValue("Period");
		firstRowSTB.createCell(7).setCellValue("DocType");
		firstRowSTB.createCell(8).setCellValue("Num images");		
		firstRowSTB.createCell(9).setCellValue("Restricted");
		firstRowSTB.createCell(10).setCellValue("Path");

		return myWorkBook;
	}
	
	static String cleanString(String str){
		str = str.toLowerCase();
		String cleaned = "";
		cleaned=str.replaceAll("�", "ae");
		cleaned=cleaned.replaceAll("�", "oe");
		cleaned=cleaned.replaceAll("�", "ss");
		cleaned=cleaned.replaceAll("�", "ue");
		cleaned=cleaned.replaceAll(" ", "-");
		return cleaned;
	}
	

	public static void createCell(XSSFWorkbook myWBook, int numPages,String id2,String externId, String dateFrom, String dateTo,String bookName,String place,String sheetName,String docType,int index){
		XSSFRow currentRow = myWBook.getSheet(sheetName).createRow(index);
		currentRow.createCell(0).setCellValue(id2);
		
		String pfarreName = cleanString(place);
		pfarreName=pfarreName.replaceAll("("+StringUtils.substringBetween(pfarreName, "(", ")")+")","");
		pfarreName=pfarreName.replaceAll("()","");
		currentRow.createCell(1).setCellValue(pfarreName);
		currentRow.createCell(2).setCellValue(externId);
		currentRow.createCell(3).setCellValue(cleanString(bookName));
		currentRow.createCell(4).setCellValue(dateFrom);
		currentRow.createCell(5).setCellValue(dateTo);
		currentRow.createCell(6).setCellValue(dateFrom+"-"+dateTo);
		currentRow.createCell(7).setCellValue(docType);
		currentRow.createCell(8).setCellValue(numPages);
		currentRow.createCell(9).setCellValue(isRestricted(docType,dateFrom,dateTo));
		currentRow.createCell(10).setCellValue("/"+docType+"/"+id2+"/"+externId);
	}

	static boolean isRestricted(String type,String dateFrom, String dateTo){
		boolean restricted=false;
		try{
			switch(type){
			case "Death":
				restricted = (Integer.parseInt(dateFrom)>1987 || Integer.parseInt(dateTo)>1987);
				break;
			case "Marriage":
				restricted = (Integer.parseInt(dateFrom)>1942 || Integer.parseInt(dateTo)>1942);
				break;
			case "Birth":
				restricted = (Integer.parseInt(dateFrom)>1917 || Integer.parseInt(dateTo)>1917);
				break;
			}}
		catch(NumberFormatException e){
			System.err.println("Number format Exception : "+dateFrom);
			restricted = false;
		}
		
		return restricted;
	}
}
